<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class w extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		$this->load->helper('url');
		$this->load->view('login');
	}
	public function login(){
		$this->load->helper('url');
		$this->load->model('modele');
		$id = $this->modele->auth($_POST['user'],$_POST['pass']);
		if (!empty($id)) {
			$data['note'] = $this->modele->notebyakun($id);
			$data['akun'] = $this->modele->akun($id);
			$this->load->view('viewe', $data);
		} else redirect(base_url(), 'refresh');
	}
	public function logout(){
		$this->load->helper('url');
		redirect(base_url(), 'refresh');
	}
	public function back($id){
		$this->load->helper('url');
		$this->load->model('modele');
		$data['note'] = $this->modele->notebyakun($id);
		$data['akun'] = $this->modele->akun($id);
		$this->load->view('viewe', $data);
    }
	public function read($id){
		$this->load->helper('url');
		$this->load->model('modele');
		$data['note'] = $this->modele->notebynote($id);
		$this->load->view('read', $data);
    }
    public function delete($id){
		$this->load->helper('url');
		$this->load->model('modele');
		$w = $this->modele->erase($id);
		$data['note'] = $this->modele->notebyakun($w);
		$data['akun'] = $this->modele->akun($w);
		$this->load->view('viewe', $data);
    }
    public function edit($id){
		$this->load->helper('url');
		$this->load->model('modele');
		$data['note'] = $this->modele->notebynote($id);
		$this->load->view('edit', $data);
    }
    public function editing(){
		$this->load->helper('url');
		$this->load->model('modele');
		$this->modele->update($_POST['id'], $_POST['judul'],$_POST['konten'],$_POST['cover']);
		$data['note'] = $this->modele->notebynote($_POST['id']);
		$this->load->view('read', $data);
    }
    public function add($id){
		$this->load->helper('url');
		$data['id'] = $id;
		$this->load->view('add', $data);
    }
    public function adding(){
		$this->load->helper('url');
		$this->load->model('modele');
		$this->modele->insert($_POST['id'],$_POST['judul'],$_POST['konten'],$_POST['thumbnail']);
		$data['note'] = $this->modele->notebyakun($_POST['id']);
		$data['akun'] = $this->modele->akun($_POST['id']);
		$this->load->view('viewe', $data);
	}
	public function create(){
		$this->load->helper('url');
		$this->load->view('daftar');
	}
	public function creating(){
		$this->load->helper('url');
		$this->load->model('modele');
		$this->modele->insertProfil($_POST['username'],$_POST['password'],$_POST['name'],$_POST['nickname'],$_POST['jk'],$_POST['no_hp'],$_POST['foto_profil']);
		redirect(base_url(), 'refresh');
	}
	public function viewProfil($id){
		$this->load->helper('url');
		$this->load->model('modele');
		$data['akun'] = $this->modele->akun($id);
		$this->load->view('profil', $data);
	}
	public function editProfil($id){
		$this->load->helper('url');
		$this->load->model('modele');
		$data['akun'] = $this->modele->akun($id);
		$this->load->view('profiledit', $data);
	}
	public function editingProfil(){
		$this->load->helper('url');
		$this->load->model('modele');
		$this->modele->updateProfil($_POST['username'],$_POST['password'],$_POST['name'],$_POST['nickname'],$_POST['jk'],$_POST['no_hp'],$_POST['foto_profil'],$_POST['id_akun']);
		$data['akun'] = $this->modele->akun($_POST['id_akun']);
		$this->load->view('profil', $data);
	}
	public function help($id){
		$this->load->helper('url');
		$data['id'] = $id;
		$this->load->view('readme', $data);
	}
}
