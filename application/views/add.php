<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG - New Note</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

        input[type=submit]{
            background-color: transparent;
			font-size: 19px;
            border:none;
        }

        input[type=submit]:hover {
            cursor:pointer;
        }

		input[type=text]{
            width:100%;
        }

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		table {
			width: 100%;
			margin-bottom: 10px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 5px 10px;
			width: 30%;
			text-align: center;
		}

		#body table, #body td{
			border-bottom: none;
			width:auto;
			text-align:left;
			padding: 0px
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
				<td style="text-align: left;"><a href="<?php echo base_url("/back/$id");?>" class="h1">← Cancel</a></td>
                <td><label class="h1">Add A Note</label></td>
                <form action="<?php echo base_url('/adding');?>" method="post" enctype="multipart/form-data">
				<td style="text-align: right;"><input type="submit" value="Next →"></td>
			</tr>
		</table>
		<div id="body">
        <table>
            <tr><td><br><label>Judul:</label></td></tr>
            <tr><td><input type="text" name="judul" autofocus required></td></tr>
            <tr><td><br><label>Konten: &emsp; *use 'break tag' for line break</label></td></tr>
            <input type="hidden" name="id" value="<?php echo $id;?>">
            <tr><td><textarea name="konten" required></textarea></td></tr>
            <tr><td><br><label>Thumbnail:</label></td></tr>
			<tr><td><input type="file" name="thumbnail"></td></tr>
        </table>
        </form>
		</div>
		<p class="footer">&copy; CICILALANG Team</p>
</body>

</html>