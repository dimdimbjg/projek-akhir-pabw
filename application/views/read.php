<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<?php foreach($note as $value):?>
	<title>CICILALANG (<?php echo $value->judul;?>)</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		.h1 img {
			width: 40px;
			height: 40px;
			border-radius: 100%;
			margin: -10px -10px -5px 5px;
			display: inline-block;
			vertical-align: middle;
			border:0.5px solid black;
		}

		table {
			width: 100%;
			margin-bottom: 10px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 9px 10px;
			width: 30%;
			text-align: center;
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
            text-align:center;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
				<td style="text-align: left;"><a href="<?php echo base_url("/back/$value->id_akun");?>" class="h1">← Back</a></td>
				<td><a style="color:red;text-decoration:underline;" href="<?php echo base_url("/delete/$value->id_note");?>" class="h1">Delete Note</a></td>
				<td style="text-align: right;"><a href="<?php echo base_url("/edit/$value->id_note");?>" class="h1">Edit →</a></td>
			</tr>
		</table>
        <div id="body"><br>
            <h2><?php echo $value->judul?></h2>
            <br><p><?php echo $value->isi?></p><br><br>
            <img src="" alt="">gambar thumbnail e, nek gatek hapus ae
            <?php endforeach;?>
        </div>
		<p class="footer">&copy; CICILALANG Team</p>
</body>

</html>