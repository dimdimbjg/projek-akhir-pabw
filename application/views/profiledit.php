<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG - New Note</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

        input[type=submit]{
            background-color: transparent;
			font-size: 19px;
            border:none;
        }

        input[type=submit]:hover {
            cursor:pointer;
        }

		input[type=text]{
            width:300px;
        }

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		table {
			width: 100%;
			margin-bottom: 10px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 5px 10px;
			width: 30%;
			text-align: center;
		}

		#body table, #body td{
			border-bottom: none;
			width:auto;
			text-align:left;
			padding: 20px 10px;
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
                <?php foreach($akun as $chara):?>
				<td style="text-align: left;"><a href="<?php echo base_url("/viewProfil/$chara->id_akun");?>" class="h1">← Cancel</a></td>
                <td><label class="h1">Profil</label></td>
				<form action="<?php echo base_url('/editingProfil');?>" method="post" enctype="multipart/form-data">
				<td style="text-align: right;"><input type="submit" value="Save →"></td>
			</tr>
		</table>
		<div id="body">
        <table>
            <tr><td>Full Name: </td><td><input type="text" name="name" value="<?php echo $chara->name;?>"></td></tr>
            <tr><td>Nickname: </td><td><input type="text" name="nickname" value="<?php echo $chara->nickname;?>"></td></tr>
            <tr><td>Gender: </td><td><input style="width: 15px;" type="radio" name="jk" value="M">Lanang&nbsp;&emsp;
            <input style="width: 15px;" type="radio" name="jk" value="W" checked>Wedok</td></tr><input type="hidden" name="id_akun" value="<?php echo $chara->id_akun;?>">
            <tr><td>Phone Number: </td><td><input type="text" name="no_hp" value="<?php echo $chara->no_hp;?>"></td></tr>
            <tr><td>Username: </td><td><input type="text" name="username" value="<?php echo $chara->username;?>"></td></tr>
            <tr><td>Password: </td><td><input type="text" name="password" value="<?php echo $chara->password;?>"></td></tr>
            <tr><td>Picture: </td><td><input type="file" name="foto_profil"></td></tr><?php endforeach;?>
        </table>
        </form>
		</div>
		<p class="footer">&copy; CICILALANG Team</p>
</body>

</html>