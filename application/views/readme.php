<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG - Support</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		.h1 img {
			width: 40px;
			height: 40px;
			border-radius: 100%;
			margin: -10px -10px -5px 5px;
			display: inline-block;
			vertical-align: middle;
			border:0.5px solid black;
		}

		table {
			width: 100%;
			margin-bottom: 10px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 9px 10px;
			width: 30%;
			text-align: center;
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
            text-align:center;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
				<td style="text-align: left;"><a href="<?php echo base_url("/back/$id");?>" class="h1">← Back</a></td>
                <td><label class="h1">Support</label></td>
				<td></td>
			</tr>
		</table>
        <div id="body"><br>
            <h2>Start Creating a Note.</h2>
            <h4>klik add a note lah.</h4><br>
            <h2>See Note or Profil</h2>
            <h4>pencet note e laaa. ndelok profil nang pojok kanan atas
            <br> ono nickname iku di klik.</h4><br>
            <h2>Edit and Delete</h2>
            <h4>nang njerone note(opo see note podo ae) bagian ndukur,
            <br> nek hapus note yo klik hapus nak ndukur tengah,
            <br> nek edit note profil yo pencet edit nak ndukur kanan.</h4><br>
            <h2>Update and Log Out</h2>
            <h4>nang njerone profil(opo see profil podo ae) bagian ndukur,
            <br> nek sign out yo klik sign out nak ndukur tengah,
            <br> nek update profil yo pencet edit nak ndukur kanan.</h4><br>
        </div>
		<p class="footer">&copy; CICILALANG Team</p>
</body>

</html>