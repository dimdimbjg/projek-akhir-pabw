<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG - New Note</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

        input[type=submit]{
            background-color: transparent;
			font-size: 19px;
            border:none;
        }

        input[type=submit]:hover {
            cursor:pointer;
        }

		input[type=text]{
            width:100%;
        }

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		table {
			width: 100%;
			margin-bottom: 10px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 5px 10px;
			width: 30%;
			text-align: center;
		}

		#body table, #body td{
			border-bottom: none;
			width:auto;
			text-align:left;
			padding: 0px
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 20px 0px 0px 10px;
			display: inline-block;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
                <?php foreach($akun as $chara):?>
				<td style="text-align: left;"><a href="<?php echo base_url("/back/$chara->id_akun");?>" class="h1">← Back</a></td>
                <td><a href="<?php echo base_url("/logout");?>" style="color:red; text-decoration: underline;" class="h1">Log Out</a></td>
				<td style="text-align: right;"><a href="<?php echo base_url("/editProfil/$chara->id_akun");?>" class="h1">Edit →</a></td>
			</tr>
		</table>
		<div id="body">
        <table>
            <tr><td>nampilno poto profil<?php echo $chara->foto_profil;?></td><td><?php echo $chara->nickname;?></td></tr>
            <tr><td></td><td>Full Name: <?php echo $chara->name;?></td></tr>
            <tr><td></td><td>Gender: <?php echo $chara->jk;?></td></tr>
            <tr><td></td><td>Phone Number: <?php echo $chara->no_hp;?></td></tr>
            <tr><td></td><td>Username: <?php echo $chara->username;?></td></tr>
            <tr><td></td><td>Password: <?php echo $chara->password;?></td></tr>
            <?php endforeach;?>
        </table>
        </form>
		</div>
		<p class="footer">&copy; CICILALANG Team</p>
</body>

</html>