<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		.h1 img {
			width: 40px;
			height: 40px;
			border-radius: 100%;
			margin: -10px -10px -5px 5px;
			display: inline-block;
			vertical-align: middle;
			border:0.5px solid black;
		}

		.card {
			font-family: Arial, Helvetica, sans-serif;
			font-size: 15px;
			text-decoration: none;
			background-color: #f9f9f9;
			border: 1px solid #D0D0D0;
			color: #002166;
			display: block;
			margin: 20px 19px 0px 0px;
			padding: 0px 0px 0px 0px;
			width: 150px;
			height: 194px;
			float: left;
		}

		.card img {
			border: 1px solid #D0D0D0;
			display: block;
			width: 150px;
			height: 150px;
			padding: 0px;
			margin: -1px 0px 0px -1px;
		}

		table {
			width: 100%;
			margin-bottom: 15px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 5px 10px;
			width: 30%;
			text-align: center;
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
				<?php foreach($akun as $value){}?>
				<td style="text-align: left;width: 60%;"><label class="h1">Welcome <?php echo $value->name;?> to CICILALANG Online Notes</label></td>
				<td style="text-align: right;"><a href="<?php echo base_url("/viewProfil/$value->id_akun");?>" class="h1"><?php echo $value->nickname;?><img src="ss.png" alt=""></a></td>
			</tr>
		</table>
		<table style="border-bottom: none;margin: -10px 0px 0px 0px;"><tr><td><a href="<?php echo base_url("/add/$value->id_akun");?>">Add New Note</a></td></tr>
		</table>
		<div id="body">
			<?php foreach($note as $value):?>
			<a href="<?php echo base_url("/read/$value->id_note");?>" class="card"><img src="" alt=""><?php echo $value->judul?></a>
			<?php endforeach;?>
		</div>
		<p class="footer">&copy; CICILALANG Team | <a href="<?php echo base_url("/help/$value->id_akun");?>">Need Help?</a></p>
</body>

</html>