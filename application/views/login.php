<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG - Log In</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		#body {
			margin: 150px 20px 150px 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<div id="body">
            <form action="<?php echo base_url("/login");?>" method="post">
                Username<br><input type="text" name="user" placeholder="Username" autofocus><br><br>
                Password<br><input type="password" name="pass" placeholder="Password" ><br><br><br>
                <input type="submit" value="Log In">
            </form>
			<br><hr><br><a href="<?php echo base_url("/create");?>">Create</a> a Profil
        </div>
    </div>
</body>

</html>