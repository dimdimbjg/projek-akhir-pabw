<!-- <?php defined('BASEPATH') OR exit('No direct script access allowed');?> -->
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>CICILALANG - Edit</title>

	<style type="text/css">
		::selection {
			background-color: #E13300;
			color: white;
		}

		::-moz-selection {
			background-color: #E13300;
			color: white;
		}

		body {
			background-color: #fff;
			margin: 40px;
			font: 13px/20px normal Helvetica, Arial, sans-serif;
			color: #4F5155;
            text-align:center;
		}

		a {
			color: blue;
			background-color: transparent;
			font-weight: normal;
			text-align: center;
		}

        input[type=submit]{
            background-color: transparent;
			font-size: 19px;
            border:none;
        }

        input[type=submit]:hover {
            cursor:pointer;
        }

		input[type=text]{
            width:100%;
        }

		.h1 {
			color: #444;
			background-color: transparent;
			text-decoration: none;
			font-size: 19px;
			font-weight: normal;
		}

		table {
			width: 100%;
			margin-bottom: 10px;
			border-bottom: 1px solid #D0D0D0;
		}

		td {
			padding: 10px 10px 5px 10px;
			width: 30%;
			text-align: center;
		}

		#body table{
			border-bottom: none;
			width:max-content;
		}

		#body td{
			width:auto;
			text-align:left;
			padding: 0px;
		}

		#body {
			margin: 0 20px 0 20px;
			padding: 0px 0px 0px 10px;
			display: inline-block;
		}

		p.footer {
			text-align: center;
			font-size: 11px;
			border-top: 1px solid #D0D0D0;
			line-height: 32px;
			padding: 0 10px 0 10px;
			margin: 20px 0 0 0;
		}

		#container {
			margin: 10px;
			border: 1px solid #D0D0D0;
			box-shadow: 0 0 8px #D0D0D0;
		}
	</style>
</head>

<body>
	<div id="container">
		<table>
			<tr>
            <?php foreach($note as $value):?>
				<td style="text-align: left;"><a href="<?php echo base_url("/read/$value->id_note");?>" class="h1">← Cancel</a></td>
                <td><label class="h1">Edit Curent Note</label></td>
                <form action="<?php echo base_url('/editing');?>" method="post" enctype="multipart/form-data">
				<td style="text-align: right;"><input type="submit" value="Save →"></td>
			</tr>
		</table>
		<div id="body">
        <table>
            <tr><td><br><label>Judul:</label></td></tr>
            <tr><td><input type="text" name="judul" value="<?php echo $value->judul;?>" autofocus required></td></tr>
            <tr><td><br><label>Konten:</label></td></tr>
            <tr><td><textarea name="konten" required><?php echo $value->isi;?></textarea></td></tr>
			<input type="hidden" name="id" value="<?php echo $value->id_note?>">
            <tr><td><br><label>Thumbnail:</label></td></tr>
			</table><table>
            <tr><td><img src="" alt="">gambar thumbnail e,<br>nek hapus yo hapus ae</td></tr>
			<tr><td><input type="file" name="cover"></td></tr>
		</table>
        <?php endforeach;?>
        </form>
		</div>
		<p class="footer">&copy; CICILALANG Team</p>
</body>

</html>