<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class modele extends CI_Model {
    public function __construct(){
        $this->load->database();
    }
    public function auth($a,$b){
        $w = $this->db->get_where('akun',array('username' => $a));
        foreach ($w->result() as $value):
            if ($b == $value->password) {
                return $value->id_akun;
            }
        endforeach;
    }
    public function akun($a){
        $w = $this->db->get_where('akun',array('id_akun' => "$a"));
        return $w->result();
    }
    public function notebyakun($a){
        $w = $this->db->get_where('note',array('id_akun' => "$a"));
        return $w->result();
    }
    public function notebynote($a){
        $w = $this->db->get_where('note',array('id_note' => "$a"));
        return $w->result();
    }
    public function update($a,$b,$c,$d){
        $data = array(
            'judul' => "$b",
            'isi' => "$c",
            'cover' => $d
        );
        $this->db->update('note',$data,"id_note=$a");
    }
    public function insert($a,$b,$c,$d){
        $data = array(
            'id_akun' => $a,
            'judul' => "$b",
            'isi' => "$c",
            'cover' => "$d"
        );
        $this->db->insert('note', $data);
    }
    public function erase($a){
        $w = $this->db->get_where('note',array('id_note' => "$a"));
        foreach ($w->result() as $value) {
            $w = $value->id_akun;
        }
        $this->db->delete('note', array('id_note' => $a));
        return $w;
    }
    public function updateProfil($a,$b,$c,$d,$e,$f,$g,$h){
        $data = array(
            'username' => "$a",
            'password' => "$b",
            'name' => "$c",
            'nickname' => "$d",
            'jk' => "$e",
            'no_hp' => "$f",
            'foto_profil' => $g
        );
        $this->db->update('akun',$data,"id_akun=$h");
    }
    public function insertProfil($a,$b,$c,$d,$e,$f,$g){
        $data = array(
            'username' => "$a",
            'password' => "$b",
            'name' => "$c",
            'nickname' => "$d",
            'jk' => "$e",
            'no_hp' => "$f",
            'foto_profil' => $g
        );
        $this->db->insert('akun',$data);
    }
}